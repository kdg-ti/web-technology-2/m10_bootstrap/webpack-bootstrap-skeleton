About
=====
Bootstrap (+ npm, webpack, sass)

The project contains a small sass file that 
outlines important page elements for testing.
Replace it with the styling for your project.

Instructies
===========
1. `npm install`
2. `npm run start`

To start your own project
=========================
- remove the .git folder  
- rename the project
