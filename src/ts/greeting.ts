const output = document.getElementById("output");

export function init() {
    document.getElementById("title")?.addEventListener("click", event => {
        if (output) {
            output.innerHTML = `
        <h2>An empty project with Bootstrap, Webpack, typescript (and Sass)</h2>
        Use this as a starting point for you project.<br>
        Remove the .git folder when doing so.`
        }
    })
}
