import HtmlWebpackPlugin from "html-webpack-plugin"
import MiniCssExtractPlugin from "mini-css-extract-plugin"

import  path from "path";
const config = {
  devtool: "source-map",
  output: {
    clean:true
  },
  mode: "development",
  plugins: [
    new HtmlWebpackPlugin({template: "./src/html/index.html"}),
    new MiniCssExtractPlugin()
  ],
  module: {
    rules: [
      {
        test: /\.ts$/,
        use: 'ts-loader',
        exclude: /node_modules/,
      },
      {test: /\.s?css$/i,
        //      use: ["style-loader", "css-loader","sass-loader"]}, // this is better in development
        use: [MiniCssExtractPlugin.loader, "css-loader","sass-loader"]},
      {test:/\.html?$/i,
        use: ['html-loader']},
      // Image assets
      {
        test: /\.(png|svg|jpe?g|gif)$/i,
        type: "asset"
      },
      // Font assets
      {
        test: /\.(woff2?|eot|ttf|otf)$/i,
        type: "asset"
      },
    ]
  },
  resolve: {
    extensions: ['.ts', '.js'],
  },
  devServer: {
    static: { directory: path.resolve( "dist")},
    open: true,
    hot: false
  }
}
export default config
